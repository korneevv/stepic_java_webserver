package accounts;

import dbService.DBException;
import dbService.DBService;
import dbService.DBServiceImpl;
import dbService.dataSets.UsersDataSet;

import java.util.HashMap;
import java.util.Map;

public class AccountService {
	private final Map<String, UserProfile> loginToProfile;
	private final Map<String, UserProfile> sessionIdToProfile;
	
	private DBService dbService = new DBServiceImpl();
	
	public AccountService() {
		loginToProfile = new HashMap<>();
		sessionIdToProfile = new HashMap<>();
	}
	
	public void addNewUser(UserProfile userProfile) {
		loginToProfile.put(userProfile.getLogin(), userProfile);
	}
	
	public UserProfile getUserByLogin(String login) {
		return loginToProfile.get(login);
	}
	
	public UserProfile getUserBySessionId(String sessionId) {
		return sessionIdToProfile.get(sessionId);
	}
	
	public void addSession(String sessionId, UserProfile userProfile) {
		sessionIdToProfile.put(sessionId, userProfile);
	}
	
	public void deleteSession(String sessionId) {
		sessionIdToProfile.remove(sessionId);
	}
	
	public void addNewUserDS(UsersDataSet usersDataSet) throws DBException {
		dbService.addUsersDS(usersDataSet.getName(), usersDataSet.getPass());
	}
	
	public UsersDataSet getUserByLoginDB(String login) throws DBException {
		return dbService.getUser(login);
	}
}
