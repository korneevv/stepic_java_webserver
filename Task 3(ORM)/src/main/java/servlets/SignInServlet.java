package servlets;

import accounts.AccountService;
import dbService.DBException;
import dbService.dataSets.UsersDataSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SignInServlet extends HttpServlet {
	private final AccountService accountService;
	
	public SignInServlet(AccountService accountService) {
		this.accountService = accountService;
	}
	
	//sign in
	public void doPost(HttpServletRequest request,
	                   HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter("login");
		String pass = request.getParameter("password");
		
		if (login == null || pass == null) {
			response.setContentType("text/html;charset=utf-8");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		UsersDataSet usersDataSet = null;//
		try {
			usersDataSet = accountService.getUserByLoginDB(login);
		} catch (DBException e) {
			e.printStackTrace();
		}
		
		if (usersDataSet == null) {
			response.setContentType("text/html;charset=utf-8");
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.getWriter().println("Unauthorized");
		} else if (usersDataSet.getName().equals(login) && usersDataSet.getPass().equals(pass)) {
			response.setContentType("text/html;charset=utf-8");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println("Authorized: " + login);
		} else {
			response.setContentType("text/html;charset=utf-8");
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.getWriter().println("Unauthorized");
		}
	}
}
