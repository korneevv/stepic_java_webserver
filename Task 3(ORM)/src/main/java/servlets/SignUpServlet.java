package servlets;

import accounts.AccountService;
import dbService.DBException;
import dbService.dataSets.UsersDataSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SignUpServlet extends HttpServlet {
	@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
	private final AccountService accountService;
	
	public SignUpServlet(AccountService accountService) {
		this.accountService = accountService;
	}
	
	//sign up
	public void doPost(HttpServletRequest request,
	                   HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter("login");
		String pass = request.getParameter("password");
		
		if (login == null || pass == null) {
			response.setContentType("text/html;charset=utf-8");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		final UsersDataSet usersDataSet = new UsersDataSet(login, pass);
		try {
			accountService.addNewUserDS(usersDataSet);//
		} catch (DBException e) {
			e.printStackTrace();
		}
	}
}
