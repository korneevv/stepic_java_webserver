package dbService;

import dbService.dataSets.UsersDataSet;

public interface DBService {

    long addUsersDS(String name, String pass) throws DBException;

    UsersDataSet getUser(String login) throws DBException;
}
