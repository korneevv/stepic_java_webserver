package dbService.dataSets;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "users")
public class UsersDataSet implements Serializable { // Serializable Important to Hibernate!
	private static final long serialVersionUID = -8706689714326132798L;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "name", unique = true, updatable = false)
	private String name;
	
	@Column(name = "pass", updatable = false)
	private String pass;
	
	//Important to Hibernate!
	@SuppressWarnings("UnusedDeclaration")
	public UsersDataSet() {
	}
	
	public UsersDataSet(String name) {
		this.setId(-1);
		this.setName(name);
	}
	
	@SuppressWarnings("UnusedDeclaration")
	public UsersDataSet(long id, String name) {
		this.setId(id);
		this.setName(name);
	}
	
	public UsersDataSet(String name, String pass) {
		setId(-1);
		setName(name);
		setPass(pass);
	}
	
	@SuppressWarnings("UnusedDeclaration")
	public String getName() {
		return name;
	}
	
	public String getPass() {
		return pass;
	}
	
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
}
