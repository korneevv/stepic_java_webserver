import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {
	static final int PORT = 5050;
	
	public static void main(String[] args) throws IOException {
		ServerSocket serverSocket = new ServerSocket(PORT);
		System.out.println("Server started");
		try {
			while (true) {
				Socket socket = serverSocket.accept();
				try {
					new ServerChat(socket);
				} catch (IOException e) {
					socket.close();
				}
			}
		} finally {
			serverSocket.close();
		}
	}
}
