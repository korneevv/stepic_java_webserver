import java.net.Socket;

public class ServerChat extends Thread {
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;
	
	public ServerChat(Socket s) throws IOException {
		socket = s;
		InputStream inputStream = socket.getInputStream();
		in = new BufferedReader(new InputStreamReader(inputStream));
		out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
		start();
	}
	
	public void run() {
		try {
			while (true) {
				String line = in.readLine();
				if ("Bue.".equals(line)) {
					break;
				}
				out.println(line);
			}
		} catch (IOException e) {
			System.err.println("IO Exception");
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				System.err.println("Socket not closed");
			}
		}
	}
}
