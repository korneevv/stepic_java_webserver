import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;

public class Main {
	private static int PORT = 5050;
	
	public static void main(String[] args) throws IOException {
		
		Charset charset = Charset.forName(System.getProperty("file.encoding"));
		ByteBuffer buffer = ByteBuffer.allocate(256);
		ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
		Selector selector = Selector.open();
		SocketChannel socketChannel = null;
		
		try {
			serverSocketChannel.configureBlocking(false);
			serverSocketChannel.socket().bind(new InetSocketAddress(PORT));
			SelectionKey key = serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
			System.out.println("Server started");
			
			while (true) {
				selector.select();
				Iterator iterator = selector.selectedKeys().iterator();
				
				while (iterator.hasNext()) {
					SelectionKey skey = (SelectionKey) iterator.next();
					
					if (skey.isAcceptable()) {
						socketChannel = serverSocketChannel.accept();
						socketChannel.configureBlocking(false);
						socketChannel.register(selector, SelectionKey.OP_READ);
						iterator.remove();
					} else if (skey.isReadable()) {
						socketChannel = (SocketChannel) skey.channel();
						socketChannel.read(buffer);
						CharBuffer charBuffer = charset.decode((ByteBuffer) buffer.flip());
						String response = charBuffer.toString();
						
						if (response.contains("Bue.")) {
							socketChannel.close();
							break;
						}
						
						socketChannel.write((ByteBuffer) buffer.rewind());
						buffer.clear();
					}
				}
			}
		} finally {
			if (socketChannel != null)
				socketChannel.close();
			serverSocketChannel.close();
			selector.close();
		}
	}
}
