package accountServer;

@SuppressWarnings("UnusedDeclaration")
public interface AccountServerControllerMBean {
    void setUsersLimit(int usersLimit);
}
