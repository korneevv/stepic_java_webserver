package accountServer;

public class AccountServerController implements AccountServerControllerMBean {
    private final AccountServerI accountServer;

    public AccountServerController(AccountServerI accountServer) {
        this.accountServer = accountServer;
    }

    @Override
    public void setUsersLimit(int limit) {
        accountServer.setUsersLimit(limit);
    }
}
